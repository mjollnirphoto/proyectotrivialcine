'use strict';

import quiz from './quiz.json' assert { type: 'json' };

/** ##  LocalStorage  ## */
localStorage.setItem('quiz', JSON.stringify(quiz));
const localStorageQuiz = JSON.parse(localStorage.getItem('quiz'));

// creamos los items si no existen
if (!localStorage.getItem('nQuestion')) {
  localStorage.setItem('nQuestion', 0);
}
if (!localStorage.getItem('aTrue')) {
  localStorage.setItem('aTrue', 0);
}

// STATE
const state = { quiz: localStorageQuiz || [] };

// Declaramos variables
let questionH1 = document.getElementById('question');
let answersUL = document.getElementById('answers');
let buttonReset = document.getElementById('reset');

// FUNCION DE RESETEO PARA "NQUESTION" Y "ATRUE"

function setStorage(item) {
  let num = Number(localStorage.getItem(item)) + 1;
  localStorage.setItem(item, num);
}

// FUNCION PARA EL CLICK DE LOS "LI"

function eventoClick(e, correct) {
  if (correct === e.target.innerText) {
    setStorage('aTrue');
    e.target.classList.add('correct');
    element.classList.remove('incorrect');
  }
  setStorage('nQuestion');

  render();
}
// FUNCION PARA CREAR LOS "UL" Y "LI"

function createLI(answers, correct) {
  answersUL.textContent = '';
  const UL = document.createElement('ul');
  for (let i = 0; i < answers.length; i++) {
    const LI = document.createElement('li');
    LI.classList.add('item');
    LI.innerHTML = answers[i];
    UL.append(LI);
    LI.addEventListener('click', (e) => eventoClick(e, correct));
  }
  answersUL.append(UL);
}

// RENDER

function render() {
  questionH1.textContent = '';
  const H1 = document.createElement('h1');
  let a = localStorage.getItem('nQuestion');

  if (a < 50) {
    let { question, answers, correct } = state.quiz[a];
    H1.innerHTML = question;
    createLI(answers, correct);
  } else {
    answersUL.textContent = '';
    H1.innerHTML = 'Puntuación Final';
    const puntuacion = localStorage.getItem('aTrue');
    answersUL.innerHTML = puntuacion;
  }
  let questionNumber = document.getElementById('question-number');

  questionNumber.innerText = Number(a) + 1;
  questionH1.append(H1);
}
render();

// FUNCION BOTON RESETEO

function reset() {
  localStorage.setItem('aTrue', 0);
  localStorage.setItem('nQuestion', 0);
  render();
}
buttonReset.addEventListener('click', reset);

/* 
const el = document.getElementById('quiz-list');
const url =
  'https://gist.githubusercontent.com/bertez/2528edb2ab7857dae29c39d1fb669d31/raw/4891dde8eac038aa5719512adee4b4243a8063fd/quiz.json';
let list = [];
async function fetchMovies(url) {
  return await fetch(url)
    .then((res) => res.json())
    .then((res) => (list = res));
}

(async () => {
  await fetchMovies(url);
  currentQuiz = list[currentQuizIndex];
  document.getElementById('question').innerText = currentQuiz.question;
  document.getElementById('botton1').innerText = currentQuiz.answers[0];
  document.getElementById('botton2').innerText = currentQuiz.answers[1];
  document.getElementById('botton3').innerText = currentQuiz.answers[2];
  document.getElementById('botton4').innerText = currentQuiz.answers[3];
})();

let currentQuizIndex = 0; // 0-49 quizes
let score = 0;
let currentQuiz = null;
*/
